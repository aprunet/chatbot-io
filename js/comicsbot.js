import md5 from "md5";
import "../css/marvel.css";

class ComicsBot {
  constructor(chatbot) {
    this.chatbot = chatbot;
    this.botInfo = {
      name: "ComicsBot",
      avatar: "./img/comicsbot.jpg",
    };
    const publicKey = "d4d59d7a21e7294359c6a7fad42d9497";
    const privateKey = "3eef7692c835e9a30062ab02cb1f3b4b4f106c64";

    const ts = new Date().getTime();
    const hash = md5(ts + privateKey + publicKey);

    const baseApiUrl = "https://gateway.marvel.com/v1/public/comics";
    this.apiUrl = `${baseApiUrl}?apikey=${publicKey}&ts=${ts}&hash=${hash}`;
  }

  processMessage(message) {
    const lowerCaseMessage = message.toLowerCase();

    if (lowerCaseMessage.includes("personnage")) {
      const superheroName = this.extractQuery(message);
      if (superheroName) {
        this.chatbot.addMessage(`Recherche d'informations sur ${superheroName.charAt(0).toUpperCase() + superheroName.slice(1)}...`, this.botInfo, new Date());
        this.fetchAPI("characters", superheroName);
      } else {
        this.chatbot.addMessage("Je suis désolé, je n'ai pas compris le nom du personnage. Pouvez-vous reformuler ?", this.botInfo, new Date());
      }
    } else if (lowerCaseMessage.includes("dessinateurs") || lowerCaseMessage.includes("créateurs")) {
      this.chatbot.addMessage("Recherche d'informations sur les créateurs...", this.botInfo, new Date());
      this.fetchAPI("creators");
    } else if (lowerCaseMessage.includes("comics")) {
      this.chatbot.addMessage("Recherche des dernières sorties de comics...", this.botInfo, new Date());
      this.fetchAPI("comics");
    } else if (lowerCaseMessage.includes("help") || lowerCaseMessage.includes("aide")) {
      this.chatbot.addMessage("Pour obtenir des informations sur un personnage, dites 'Personnage' suivi du nom du personnage. Pour obtenir des informations sur les créateurs d'oeuvres Marvel, dites 'Créateurs'. Pour obtenir des informations sur les dernières sorties de comics, dites 'Comics'.  Pour obtenir plus d'informations sur moi, dites 'Whois'.", this.botInfo, new Date());
    } else if (lowerCaseMessage.includes("whois")) {
      this.chatbot.addMessage("Je suis le bot Comics. Je peux vous aider à trouver des informations sur les personnages, les créateurs et les comics Marvel.", this.botInfo, new Date());
    } else {
      return;
    }
    this.chatbot.scrollToBottom();
  }

  extractQuery(message) {
    const queryMatch = message.match(/personnage (.*)/i);
    return queryMatch ? queryMatch[1].trim() : null;
  }

  fetchAPI(req, query) {
    const publicKey = "d4d59d7a21e7294359c6a7fad42d9497";
    const privateKey = "3eef7692c835e9a30062ab02cb1f3b4b4f106c64";

    const ts = new Date().getTime();
    const hash = md5(ts + privateKey + publicKey);

    const baseComicsUrl = "https://gateway.marvel.com/v1/public/comics";
    const baseCharactersUrl = "https://gateway.marvel.com/v1/public/characters";
    const baseCreatorsUrl = "https://gateway.marvel.com/v1/public/creators";

    let url;

    if (req === "comics") {
      url = `${baseComicsUrl}?apikey=${publicKey}&ts=${ts}&hash=${hash}`;
    } else if (req === "characters" && query) {
      url = `${baseCharactersUrl}?name=${encodeURIComponent(query)}&apikey=${publicKey}&ts=${ts}&hash=${hash}`;
    } else if (req === "creators") {
      url = `${baseCreatorsUrl}?apikey=${publicKey}&ts=${ts}&hash=${hash}`;
    } else {
      return;
    }

    fetch(url, {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Accept-Encoding": "gzip",
      },
    })
      .then((response) => response.json())
      .then((data) => {
        if (data) {
          data = data.data;
          if (data.results.length > 0) {
            console.log(data.results);
            if (req === "comics") {
              this.chatbot.addMessage(data.results, this.botInfo, new Date(), true, "comics");
            } else if (req === "characters") {
              this.chatbot.addMessage(data.results, this.botInfo, new Date(), true, "character");
            } else if (req === "creators") {
              this.chatbot.addMessage(data.results, this.botInfo, new Date(), true, "creator");
            }
          } else {
            this.chatbot.addMessage("Aucun personnage n'a été trouvé.", this.botInfo, new Date());
          }
        }
      })
      .catch((error) => {
        console.error("Error fetching data from Marvel API:", error);
      });
  }
}

export default ComicsBot;
