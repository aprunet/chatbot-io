import "../css/tmdb.css";

class FilmsBot {
  constructor(chatbot) {
    this.chatbot = chatbot;
    this.botInfo = {
      name: "FilmsBot",
      avatar: "./img/filmsbot.jpg",
    };
    this.apiKey = "3b9de8cc5eda1b208b8e3b30f54fa188";
    this.movieList = [];
  }

  processMessage(message) {
    const lowerCaseMessage = message.toLowerCase();
    if (lowerCaseMessage.includes("meilleurs films")) {
      const year = this.extractYear(lowerCaseMessage);
      if (year && year <= new Date().getFullYear()) {
        this.chatbot.addMessage(`Voici les films les mieux notés pour l'année ${year} :`, this.botInfo, new Date());
        this.fetchMovies(year);
      } else {
        this.chatbot.addMessage("Désolé, je n'ai pas compris l'année. Précisez l'année dans votre demande pour obtenir la liste des meilleurs films.", this.botInfo, new Date());
      }
      
    } else if (lowerCaseMessage.includes("meilleures séries tv")) {
      const year = this.extractYear(lowerCaseMessage);
      if (year && year <= new Date().getFullYear()) {
        this.chatbot.addMessage(`Voici les séries TV les mieux notées pour l'année ${year} :`, this.botInfo, new Date());
        this.fetchSeries(year);
      } else {
        this.chatbot.addMessage("Désolé, je n'ai pas compris l'année. Précisez l'année dans votre demande pour obtenir la liste des meilleures séries TV.", this.botInfo, new Date());
      }
      
    } else if (lowerCaseMessage.includes("tendances")) {
      this.chatbot.addMessage(`Voici les oeuvres cinématographiques populaires du moment :`, this.botInfo, new Date());
      this.fetchTrending();
      
    } else if (lowerCaseMessage.includes("help") || lowerCaseMessage.includes("aide")) {
      this.chatbot.addMessage("Pour obtenir les films les mieux notés d'une année, dites 'Meilleurs films' suivi de l'année. Pour obtenir les séries TV les mieux notées d'une année, dites 'Meilleures séries TV' suivi de l'année. Pour obtenir les oeuvres cinématographiques populaires du moment, dites 'Tendances'. Pour obtenir plus d'informations sur moi, dites 'Whois'.", this.botInfo, new Date());
      
    } else if (lowerCaseMessage.includes("whois")) {
      this.chatbot.addMessage("Je suis le bot Films. Je peux vous aider à trouver des informations sur les films, les séries TV et les tendances cinématographiques.", this.botInfo, new Date());
      
    } else {
      return;
    }
    this.chatbot.scrollToBottom();
  }

  extractYear(message) {
    const yearMatch = message.match(/(\d{4})/);
    return yearMatch ? parseInt(yearMatch[0]) : null;
  }

  fetchMovies(year) {
    fetch(
      `https://api.themoviedb.org/3/discover/movie?api_key=${this.apiKey}&sort_by=popularity.desc&year=${year}`
    )
      .then((response) => response.json())
      .then((data) => {
        this.movieList = data.results.slice(0, 5);
        this.displayMovies();
      })
      .catch((error) => {
        console.error("Error fetching movies:", error);
      });
  }

  displayMovies() {
    const movieData = [];
  
    this.movieList.forEach((movie) => {  
      movieData.push({
        poster_path: movie.poster_path,
        title: movie.title,
      });
    });
  
    this.chatbot.addMessage(movieData, this.botInfo, new Date(), true, "movie");
  }
  

  fetchSeries(year) {
    const url = `https://api.themoviedb.org/3/discover/tv?api_key=${this.apiKey}&language=fr-FR&sort_by=vote_average.desc&first_air_date_year=${year}&vote_count.gte=10`;
    fetch(url)
      .then((response) => response.json())
      .then((data) => {
        this.seriesList = data.results;
        this.displaySeries();
      });
  }

  displaySeries() {
    const seriesData = [];

    this.seriesList.forEach((series) => {
      seriesData.push({
        poster_path: series.poster_path,
        name: series.name,
      });
    });

    this.chatbot.addMessage(seriesData, this.botInfo, new Date(), true, "series");
  }

  fetchTrending() {
    fetch(
      `https://api.themoviedb.org/3/trending/all/day?api_key=${this.apiKey}`
    )
      .then((response) => response.json())
      .then((data) => {
        this.trendingList = data.results.slice(0, 5);
        this.displayTrending();
      });
  }

  displayTrending() {
    const moviesData = [];

    this.trendingList.forEach((movie) => {
      moviesData.push({
        poster_path: movie.poster_path,
        title: movie.title || movie.name,
      });
    });

    this.chatbot.addMessage(moviesData, this.botInfo, new Date(), true, "movie");
  }
}

export default FilmsBot;
