class ChatBot {
    constructor(bots = {}) {
        if (ChatBot.instance) {
            return ChatBot.instance;
        }
        this.messages = [];
        this.userInput = "";
        this.bots = bots;

        window.onload = () => {
            const savedMessages = localStorage.getItem("messages");
            if (savedMessages) {
                this.messages = JSON.parse(savedMessages);
                console.log(this.messages);
                this.messages.forEach((message) => {
                    if (message.type === "text") {
                        this.displayTextMessage(message.content, message.sender, new Date(message.timestamp));
                    } else if (message.type === "movie") {
                        this.displayMoviesMessage(message.content);
                    } else if (message.type === "character") {
                        this.displayCharactersMessage(message.content);
                    } else if (message.type === "series") {
                        this.displaySeriesMessage(message.content);
                    } else if (message.type === "comics") {
                        this.displayComicsMessage(message.content);
                    } else if (message.type === "creator") {
                        this.displayCreatorsMessage(message.content);
                    }
                });
            }

            document.getElementById("sendButton").addEventListener("click", () => this.handleSend());
            document.getElementById("userInput").addEventListener("keydown", (event) => {
                if (event.key === "Enter") {
                    this.handleSend();
                }
            });
        };

        ChatBot.instance = this;
    }

    static getInstance(bots) {
        if (!ChatBot.instance) {
            ChatBot.instance = new ChatBot(bots);
        }
        return ChatBot.instance;
    }

    setBots(bots) {
        this.bots = bots;
    }

    handleSend() {
        console.log("handleSend");
        const userInputElem = document.getElementById("userInput");
        const userInput = userInputElem.value.trim();
        if (!userInput) return;
        this.addMessage(userInput, "user", new Date());
        this.processMessage(userInput);
        userInputElem.value = "";
    }

    processMessage(message) {
        console.log("Processing message:", message);
        Object.values(this.bots).forEach(bot => {
            console.log("Bot:", bot);
            bot.processMessage(message)
        });
    }

    addMessage(content, sender, timestamp, saveMsg = true, type = "text") {
        console.log("Adding message:", content, sender, timestamp);
        if (saveMsg) {
            const message = {
                content,
                sender,
                timestamp: timestamp.toISOString(),
                type,
            };
            this.messages.push(message);
            localStorage.setItem("messages", JSON.stringify(this.messages));
            console.log(this.messages);
        }

        if (type === "text") {
            this.displayTextMessage(content, sender, timestamp);
        } else if (type === "movie") {
            this.displayMoviesMessage(content);
        } else if (type === "character") {
            this.displayCharactersMessage(content, sender);
        } else if (type === "series") {
            this.displaySeriesMessage(content);
        } else if (type === "comics") {
            this.displayComicsMessage(content);
        } else if (type === "creator") {
            this.displayCreatorsMessage(content);
        } 
    }

    displayCharactersMessage(characterData) {
        console.log("Displaying characters:", characterData);
        const messagesElem = document.getElementById("messages");
        const characterContainer = document.createElement("div");
        characterContainer.className = "message-container bot-message character-list";

        characterData.forEach((character) => {
            const characterItem = document.createElement("div");
            characterItem.className = "message character-item";

            const characterThumbnail = document.createElement("img");
            characterThumbnail.className = "character-thumbnail";
            characterThumbnail.src = `${character.thumbnail.path}.${character.thumbnail.extension}`;
            characterThumbnail.alt = character.name;
            characterItem.appendChild(characterThumbnail);
            characterThumbnail.onload = () => {
                this.scrollToBottom();
            };

            const characterName = document.createElement("h2");
            characterName.className = "character-name";
            characterName.textContent = character.name;
            characterItem.appendChild(characterName);

            const characterDescription = document.createElement("div");
            characterDescription.className = "character-description";
            characterDescription.textContent = character.description;
            characterItem.appendChild(characterDescription);

            const mainContainer = document.createElement("div");
            mainContainer.className = "main-container";
            characterItem.appendChild(mainContainer);

            const buttonsContainer = document.createElement("div");
            buttonsContainer.className = "buttons-container";
            mainContainer.appendChild(buttonsContainer);

            const listDisplayContainer = document.createElement("div");
            listDisplayContainer.className = "list-display-container";
            mainContainer.appendChild(listDisplayContainer);

            const toggleButtonState = (button, items) => {
                const isActive = button.classList.contains("active");

                const allButtons = buttonsContainer.querySelectorAll("button");
                allButtons.forEach(btn => btn.classList.remove("active"));

                if (!isActive) {
                    button.classList.add("active");
                    this.showCategoryList(listDisplayContainer, items);
                } else {
                    listDisplayContainer.innerHTML = '';
                }
            };

            const addCategoryButton = (category, items) => {
                const button = document.createElement("button");
                button.className = `${category}-title category-title`;
                button.textContent = category.charAt(0).toUpperCase() + category.slice(1);
                button.addEventListener("click", () => {
                    toggleButtonState(button, items);
                });
                buttonsContainer.appendChild(button);
            };

            addCategoryButton("comics", character.comics.items);
            addCategoryButton("séries", character.series.items);
            addCategoryButton("histoires", character.stories.items);
            addCategoryButton("événements", character.events.items);

            characterContainer.appendChild(characterItem);
        });

        messagesElem.appendChild(characterContainer);
    }

    showCategoryList(container, items) {
        container.innerHTML = '';
        const itemList = document.createElement("div");
        itemList.className = "category-list";
        items.forEach((item) => {
            const itemElem = document.createElement("div");
            itemElem.className = "category-item";

            const itemLink = document.createElement("span");
            itemLink.className = "category-link";
            itemLink.textContent = item.name;
            itemElem.appendChild(itemLink);

            itemList.appendChild(itemElem);
        });
        container.appendChild(itemList);
    }

    displayCreatorsMessage(creatorData) {
        console.log("Displaying creators:", creatorData);
        const messagesElem = document.getElementById("messages");
        const creatorsContainer = document.createElement("div");
        creatorsContainer.className = "message bot-message creator-list";

        creatorData.forEach((creator) => {
            const creatorItem = document.createElement("div");
            creatorItem.className = "creator-item";
            creatorsContainer.appendChild(creatorItem);

            const creatorThumbnail = document.createElement("img");
            creatorThumbnail.src = `${creator.thumbnail.path}.${creator.thumbnail.extension}`;
            creatorThumbnail.alt = creator.fullName;
            creatorThumbnail.className = "creator-thumbnail";
            creatorItem.appendChild(creatorThumbnail);
            creatorThumbnail.onload = () => {
                this.scrollToBottom();
            };
            
            const creatorName = document.createElement("div");
            creatorName.textContent = creator.fullName;
            creatorName.className = "creator-name";
            creatorItem.appendChild(creatorName);
        });

        messagesElem.appendChild(creatorsContainer);
    }

    displayComicsMessage(comicData) {
        const messagesElem = document.getElementById("messages");
        const comicsContainer = document.createElement("div");
        comicsContainer.className = "message bot-message comic-list";

        comicData.forEach((comic) => {
            const comicItem = document.createElement("div");
            comicItem.className = "comic-item";
            comicsContainer.appendChild(comicItem);

            const comicThumbnail = document.createElement("img");
            comicThumbnail.src = `${comic.thumbnail.path}.${comic.thumbnail.extension}`;
            comicThumbnail.alt = comic.title;
            comicThumbnail.className = "comic-thumbnail";
            comicItem.appendChild(comicThumbnail);
            comicThumbnail.onload = () => {
                this.scrollToBottom();
            };

            const comicTitle = document.createElement("div");
            comicTitle.textContent = comic.title;
            comicTitle.className = "comic-title";
            comicItem.appendChild(comicTitle);
        });
        
        messagesElem.appendChild(comicsContainer);
    }


    displayTextMessage(text, sender, timestamp) {
        const messageContainer = document.createElement("div");
        messageContainer.className = `message-container ${sender === "user" ? "user-message" : "bot-message"}`;
        if (sender !== "user") {
            const avatar = document.createElement("img");
            avatar.src = sender.avatar;
            avatar.alt = sender.name;
            avatar.className = "avatar";
            messageContainer.appendChild(avatar);
        }
        const messageNameTextAndTime = document.createElement("div");
        messageNameTextAndTime.className = "message-name-text-and-time";
        messageContainer.appendChild(messageNameTextAndTime);

        if (sender !== "user") {
            const nameOfBot = document.createElement("div");
            nameOfBot.className = "name-of-bot";
            nameOfBot.textContent = sender.name;
            messageNameTextAndTime.appendChild(nameOfBot);
        }

        const messageElem = document.createElement("div");
        messageElem.className = "message";
        messageElem.textContent = text;
        messageNameTextAndTime.appendChild(messageElem);

        const timeOfMessage = document.createElement("div");
        timeOfMessage.className = "time-of-message";
        timeOfMessage.textContent = this.formatDate(new Date(timestamp));
        messageNameTextAndTime.appendChild(timeOfMessage);

        const messagesElem = document.getElementById("messages");
        messagesElem.appendChild(messageContainer);
        this.scrollToBottom();
    }

    displayMoviesMessage(movieData) {
        const messagesElem = document.getElementById("messages");
        const moviesContainer = document.createElement("div");
        moviesContainer.className = "message bot-message movie-list";

        movieData.forEach((movie) => {
            const movieContainer = document.createElement("div");
            movieContainer.className = "movie-item";

            const moviePoster = document.createElement("img");
            if (movie.poster_path === null) {
                moviePoster.src = "https://incakoala.github.io/top9movie/film-poster-placeholder.png";
            } else {
                moviePoster.src = `https://image.tmdb.org/t/p/w500${movie.poster_path}`;
            }
            moviePoster.alt = movie.title;
            moviePoster.onload = () => {
                this.scrollToBottom();
            };

            const movieTitle = document.createElement("span");
            movieTitle.textContent = movie.title;

            movieContainer.appendChild(moviePoster);
            movieContainer.appendChild(movieTitle);

            moviesContainer.appendChild(movieContainer);
        });

        messagesElem.appendChild(moviesContainer);
    }

    displaySeriesMessage(seriesData) {
        const messagesElem = document.getElementById("messages");
        const seriesContainer = document.createElement("div");
        seriesContainer.className = "message bot-message series-list";

        seriesData.forEach((series) => {
            const seriesItem = document.createElement("div");
            seriesItem.className = "series-item";
            seriesContainer.appendChild(seriesItem);

            const seriesPoster = document.createElement("img");
            seriesPoster.src = `https://image.tmdb.org/t/p/w185${series.poster_path}`;
            seriesPoster.alt = series.name;
            seriesPoster.className = "series-poster";
            seriesItem.appendChild(seriesPoster);
            seriesPoster.onload = () => {
                this.scrollToBottom();
            };

            const seriesName = document.createElement("div");
            seriesName.textContent = series.name;
            seriesName.className = "series-name";
            seriesItem.appendChild(seriesName);
        });

        messagesElem.appendChild(seriesContainer);
    }


    scrollToBottom() {
        const messagesElem = document.getElementById("messages");
        messagesElem.scrollTop = messagesElem.scrollHeight;
    }

    formatDate(date) {
        const hours = date.getHours().toString().padStart(2, "0");
        const minutes = date.getMinutes().toString().padStart(2, "0");
        const day = date.getDate().toString().padStart(2, "0");
        const month = (date.getMonth() + 1).toString().padStart(2, "0");
        const year = date.getFullYear();
        return `${hours}:${minutes} - ${day}/${month}/${year}`;
    }
}

export default ChatBot;
