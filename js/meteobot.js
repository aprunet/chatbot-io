class MeteoBot {
  constructor(chatbot) {
    this.chatbot = chatbot;
    this.botInfo = {
      name: "MeteoBot",
      avatar: "./img/meteobot.jpg",
    };
    this.range = (start, stop, step) =>
      Array.from({ length: (stop - start) / step + 1 }, (_, i) => start + i * step);
  }

  handleInputChange(event) {
    this.userInput = event.target.value;
  }

  processMessage(message) {
    const lowerCaseMessage = message.toLowerCase();

    if (lowerCaseMessage.includes("météo")) {
      let city = message.split("météo")[1].trim();
      if (city === "") {
        this.chatbot.addMessage("De quelle ville voulez-vous la météo ?", this.botInfo, new Date());
      } else {
        city = city.charAt(0).toUpperCase() + city.slice(1);
        this.getCoordinates(city)
          .then((coordinates) => {
            if (coordinates) {
              this.fetchCurrentWeather(coordinates, city);
            }
          })
          .catch((error) => {
            console.error("Error getting coordinates:", error);
          });
      }
    } else if (lowerCaseMessage.includes("maximale")) {
      let city = message.split("maximale")[1].trim();
      if (city === "") {
        this.chatbot.addMessage("De quelle ville voulez-vous la prévision maximale ?", this.botInfo, new Date());
      } else {
        city = city.charAt(0).toUpperCase() + city.slice(1);
        this.getCoordinates(city)
          .then((coordinates) => {
            if (coordinates) {
              this.fetchForecast(coordinates, city);
            }
          })
          .catch((error) => {
            console.error("Error getting coordinates:", error);
          });
      }
    } else if (lowerCaseMessage.includes("semaine")) {
      let city = message.split("semaine")[1].trim();
      if (city === "") {
        this.chatbot.addMessage("De quelle ville voulez-vous les prévisions pour la semaine ?", this.botInfo, new Date());
      } else {
        city = city.charAt(0).toUpperCase() + city.slice(1);
        this.getCoordinates(city)
          .then((coordinates) => {
            if (coordinates) {
              this.fetchWeekForecast(coordinates, city);
            }
          })
          .catch((error) => {
            console.error("Error getting coordinates:", error);
          });
      }
    } else if(lowerCaseMessage.includes("help") || lowerCaseMessage.includes("aide")) {
      this.chatbot.addMessage("Pour obtenir la météo actuelle, dites 'Météo' suivi du nom de la ville. Pour obtenir la prévision maximale, dites 'Maximale' suivi du nom de la ville. Pour obtenir les prévisions de la semaine, dites 'Semaine' suivi du nom de la ville. Pour obtenir plus d'informations sur moi, dites 'Whois'.", this.botInfo, new Date());
    } else if (lowerCaseMessage.includes("whois")) {
      this.chatbot.addMessage("Je suis le bot Météo. Je peux vous aider à obtenir les prévisions pour une ville donnée.", this.botInfo, new Date());
    } else {
      return;
    }
    this.chatbot.scrollToBottom();
  }

  getCoordinates(city) {
    const baseUrl = "https://nominatim.openstreetmap.org/search";
    const params = `q=${encodeURIComponent(city)}&format=json`;

    return fetch(`${baseUrl}?${params}`)
      .then((response) => response.json())
      .then((data) => {
        if (data && data.length > 0) {
          const coordinates = { lat: data[0].lat, lon: data[0].lon };
          return coordinates;
        } else {
          this.chatbot.addMessage(
            `La localisation de ${city} n'a pas pu être trouvée.`,
            this.botInfo,
            new Date()
          );
          return null;
        }
      })
      .catch((error) => {
        console.error("Error fetching location data:", error);
        this.chatbot.addMessage(
          "Une erreur s'est produite lors de la récupération des données de localisation.",
          this.botInfo,
          new Date()
        );
        return null;
      });
  }

  fetchCurrentWeather(coordinates, city) {
    fetch(
      `https://api.open-meteo.com/v1/forecast?latitude=${coordinates.lat}&longitude=${coordinates.lon}&hourly=temperature_2m`
    )
      .then((response) => response.json())
      .then((data) => {
        const hourly = data.hourly;
        if (hourly && hourly.time && hourly.temperature_2m) {
          const currentDateTime = new Date();
          const currentIndex = this.findClosestIndex(hourly.time, currentDateTime.toISOString());

          if (currentIndex !== -1) {
            let currentTemperature = hourly.temperature_2m[currentIndex];
            currentTemperature = Math.round(currentTemperature);
            this.chatbot.addMessage(
              `À ${city}, il fait actuellement ${currentTemperature}°C.`,
              this.botInfo,
              new Date()
            );
          } else {
            this.chatbot.addMessage(
              "Les données météorologiques actuelles ne sont pas disponibles pour cette localisation.",
              this.botInfo,
              new Date()
            );
          }
        } else {
          this.chatbot.addMessage(
            "Les données météorologiques actuelles ne sont pas disponibles pour cette localisation.",
            this.botInfo,
            new Date()
          );
        }
      })
      .catch((error) => {
        console.error("Error fetching weather data:", error);
        this.chatbot.addMessage(
          "Une erreur s'est produite lors de la récupération des données météorologiques.",
          this.botInfo,
          new Date()
        );
      });
  }

  fetchForecast(coordinates, city) {
    fetch(
      `https://api.open-meteo.com/v1/forecast?latitude=${coordinates.lat}&longitude=${coordinates.lon}&daily=temperature_2m_max`
    )
      .then((response) => response.json())
      .then((data) => {
        const daily = data.daily;
        if (daily && daily.time && daily.temperature_2m_max) {
          const currentDateTime = new Date();
          const currentIndex = this.findClosestIndex(daily.time, currentDateTime.toISOString());

          if (currentIndex !== -1) {
            let maxTemperature = daily.temperature_2m_max[currentIndex];
            maxTemperature = Math.round(maxTemperature);
            this.chatbot.addMessage(
              `À ${city}, la température maximale prévue est de ${maxTemperature}°C aujourd'hui.`,
              this.botInfo,
              new Date()
            );
          } else {
            this.chatbot.addMessage(
              "Les prévisions météorologiques ne sont pas disponibles pour cette localisation.",
              this.botInfo,
              new Date()
            );
          }
        } else {
          this.chatbot.addMessage(
            "Les prévisions météorologiques ne sont pas disponibles pour cette localisation.",
            this.botInfo,
            new Date()
          );
        }
      })
      .catch((error) => {
        console.error("Error fetching weather data:", error);
        this.chatbot.addMessage(
          "Une erreur s'est produite lors de la récupération des données météorologiques.",
          this.botInfo,
          new Date()
        );
      });
  }

  fetchWeekForecast(coordinates, city) {
    fetch(
      `https://api.open-meteo.com/v1/forecast?latitude=${coordinates.lat}&longitude=${coordinates.lon}&daily=temperature_2m_max`
    )
      .then((response) => response.json())
      .then((data) => {
        const daily = data.daily;
        if (daily && daily.time && daily.temperature_2m_max) {
          const currentDateTime = new Date();
          const currentIndex = this.findClosestIndex(daily.time, currentDateTime.toISOString());

          if (currentIndex !== -1) {
            const weekForecast = daily.temperature_2m_max.slice(currentIndex, currentIndex + 7);
            const days = this.range(0, 6, 1).map((i) => {
              const date = new Date(daily.time[currentIndex + i]);
              return `${date.toLocaleDateString("fr-FR", {
                weekday: "long",
              })} : ${Math.round(weekForecast[i])}°C`;
            });
            this.chatbot.addMessage(`Prévisions pour la semaine à ${city} : ${days.join(", ")}`, this.botInfo, new Date());
          } else {
            this.chatbot.addMessage(
              "Les prévisions météorologiques de la semaine ne sont pas disponibles pour cette localisation.",
              this.botInfo,
              new Date()
            );
          }
        } else {
          this.chatbot.addMessage(
            "Les prévisions météorologiques de la semaine ne sont pas disponibles pour cette localisation.",
            this.botInfo,
            new Date()
          );
        }
      })
      .catch((error) => {
        console.error("Error fetching weather data:", error);
        this.chatbot.addMessage(
          "Une erreur s'est produite lors de la récupération des données météorologiques.",
          this.botInfo,
          new Date()
        );
      });
  }

  findClosestIndex(timeArray, targetDateTime) {
    const targetTime = new Date(targetDateTime).getTime();
    const times = timeArray.map((time) => new Date(time).getTime());

    let closestIndex = -1;
    let smallestDiff = Infinity;

    times.forEach((time, index) => {
      const diff = Math.abs(time - targetTime);
      if (diff < smallestDiff) {
        smallestDiff = diff;
        closestIndex = index;
      }
    });

    return closestIndex;
  }
}

export default MeteoBot;
