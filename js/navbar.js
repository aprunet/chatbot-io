import '../css/navbar.css';

export default class Navbar {
  constructor() {
    this.render();
  }

  render() {
    const navbar = document.createElement('div');
    navbar.className = 'navbar';

    const title = document.createElement('div');
    title.className = 'title';
    title.textContent = 'chatbot-io';

    navbar.appendChild(title);

    document.getElementById('navbar').appendChild(navbar);
  }
}
