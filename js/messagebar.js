import { library, dom } from "@fortawesome/fontawesome-svg-core";
import { faPaperPlane } from "@fortawesome/free-solid-svg-icons";
import "../css/messagebar.css";

library.add(faPaperPlane);
dom.watch();

export default class Messagebar {
  constructor() {
    this.render();
  }

  render() {
    const messagebar = document.createElement("div");
    messagebar.className = "input-area";

    const input = document.createElement("input");
    input.className = "input";
    input.id = "userInput";
    input.placeholder = "Écrivez votre message...";

    const sendButton = document.createElement("button");
    sendButton.className = "send-button";
    sendButton.id = "sendButton";
    sendButton.innerHTML = '<i class="fas fa-paper-plane"></i>';

    messagebar.appendChild(input);
    messagebar.appendChild(sendButton);

    document.getElementById("input-area").appendChild(messagebar);
  }
}
