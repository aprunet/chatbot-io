import '../css/style.css';
import '../css/navbar.css';
import Navbar from './navbar.js';
import Messagebar from './messagebar.js';
import ChatBot from './chatbot.js';
import FilmsBot from './filmsbot.js';
import ComicsBot from './comicsbot.js';
import MeteoBot from './meteobot.js';

document.addEventListener('DOMContentLoaded', () => {
  const navbarContainer = document.getElementById('navbar');
  const chatbotContainer = document.getElementById('chatbot-container');
  const messagebarContainer = document.getElementById('input-area');

  if (navbarContainer) {
    new Navbar();
  }

  if (chatbotContainer) {
    const chatbot = ChatBot.getInstance({});
    const films = new FilmsBot(chatbot);
    const comics = new ComicsBot(chatbot);
    const meteo = new MeteoBot(chatbot);

    chatbot.setBots({ films, comics, meteo });
  }

  if (messagebarContainer) {
    new Messagebar((message) => {
      console.log(message);
    });
  }
});
